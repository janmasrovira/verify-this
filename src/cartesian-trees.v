From mathcomp Require Import all_ssreflect.

Set Diffs "on".

Record Zipper (a : Type) : Type :=
  zipper {
      stack : seq a;
      value : seq a
    }.
Arguments zipper {a}.
Arguments stack {a}.
Arguments value {a}.

Generalizable All Variables.

Definition fromList `(l : seq a) : Zipper a :=
  zipper [::] l.

Definition toList `(z : Zipper a) : seq a :=
  rev (stack z) ++ (value z) .

Definition right `(z : Zipper a) : option (Zipper a) :=
  match z with
    | zipper stack nil => None
    | zipper stack (a :: b) => Some (zipper (a :: stack) b)
  end.

Definition zleft `(z : Zipper a) : option (Zipper a) :=
  match z with
    | zipper nil _ => None
    | zipper (a :: stack) val => Some (zipper stack (a :: val))
  end.

Definition index : Type := nat.
Definition iseq (a : Type) : Type := seq (index * a).

Fixpoint indexed' {a : Type} (l : seq a) (i : index) : iseq a :=
  match l with
    | nil => [::]
    | (h :: hs) => (i , h) :: indexed' hs (S i)
  end.

Definition indexed {a : Type} (l : seq a) : iseq a :=
  indexed' l 0.

Fixpoint nth {a : Type} (l : seq a) (ix : index) : option a :=
  match l , ix with
    | [::] , _ => None
    | (x :: xs) , 0 => Some x
    | (_ :: xs) , (S n) => nth xs n
  end.

Fixpoint nth1 {a : Type} (l : seq a) (ix : index) : option a :=
  match ix with
    | 0 => None
    | S n => nth l n
  end.

Fixpoint inits {a : Type} (st vs : seq a) : seq (a * seq a) :=
  match vs with
  | [::] => [::]
  | (h :: hs) => (h , st) :: inits (h :: st) hs
end.

Fixpoint dropWhile {a : Type} (f : a -> bool) (l : seq a) : seq a :=
  match l with
    | [::] => [::]
    | (b :: bs) => if f b then dropWhile f bs else b :: bs
  end.

Definition findLeft (e : nat) (l : seq nat) : nat :=
  size (dropWhile (fun n => n >= e) l).

Fixpoint leftLess (l : seq nat) :=
  map (fun '(e , l) => findLeft e l) (inits [::] l).

Definition left (i : index) (l : seq nat) : option nat :=
  nth1 (leftLess l) i.

Definition lst : seq nat := 4 :: 7 :: 8 :: 1 :: 2 :: 3 :: 9 :: 5 :: 6 :: [::].

Eval compute in inits [::] lst.
Eval compute in leftLess lst.

Definition fromOption {A : Type} (a : A) (o : option A) : A :=
  match o with
    | None => a
    | Some b => b
  end.

Lemma sizeDropWhile {A} {f} (l : seq A) : size (dropWhile f l) <= size l.
Proof using Type.
  elim l => [//|a k h //=].
   case: ifP => [fa|nfa //].
   apply: (leq_trans h (leqnSn _)).
Qed.


Lemma p1 (l : seq nat) (i : nat) (p : 1 <= i <= size l) : fromOption 0 (left i l) < i.

(* Lemma inIndexed {a : Type} {hs : seq a} {ix i0 i0' : index} : *)
(*   i0' <= i0 -> ix \in map fst (indexed' hs i0) -> i0' <= ix. *)
(*   move: i0 ix. *)
(*   (* Unset Printing Notations. *) *)
(*   elim hs => [//=| e l ih i0 ix le ]. *)
(*   move => /orP[/eqP ->// | //= k ]. *)
(*   apply: ih. *)


(* Lemma thmIx' {a : eqType} (l : seq a) (i : index) : *)
(*   all (fun '(ix , e) => nth l (ix - i) == Some e) (indexed' l i). *)
(* Proof. *)
(*   move: i. *)
(*   elim l => [//| h hs ih i //=]. *)
(*   apply/andP. split => [//|]. *)
(*     by rewrite subnn //. *)
(*     apply/allP. *)
(*     move => [ix e] inn. *)

(*     About allP. *)
(*   Check (ih (S i)). *)
(*   Locate subn. *)


(* Lemma thmIx {a : eqType} (l : seq a) : *)
(*   all (fun '(ix , e) => nth l ix == Some e) (indexed l). *)
(* Proof. *)
(*   elim l => [//| h hs ih //=]. *)
(*   apply/andP. split => [//|]. *)

(* Print nth. *)
